/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-prettier/skip-formatting'
  ],
  rulrs: {
    'vue/multi-word-component-names': 'off'
  },
  // 关闭名称校验

  parserOptions: {
    ecmaVersion: 'latest'
  }
}
