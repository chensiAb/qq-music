import { createRouter, createWebHistory } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import layout from '../layout/index.vue'
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: layout,
            children: [
                {
                    path: '/',
                    name: 'recommend',
                    component: () => import('../views/recommend/index.vue')
                },
                {
                    path: '/musicHall',
                    name: 'musicHall',
                    component: () => import('../views/musicHall/index.vue')
                },
                {
                    path: '/video',
                    name: 'video',
                    component: () => import('../views/video/indexV.vue')
                },
                {
                    path: '/radar',
                    name: 'radar',
                    component: () => import('../views/radar/index.vue')
                },
                {
                    path: '/myLove',
                    name: 'myLove',
                    component: () => import('../views/myLove/index.vue')
                },
                {
                    path: '/downLoad',
                    name: 'downLoad',
                    component: () => import('../views/downLoad/index.vue')
                },
                {
                    path: '/recentlyPlay',
                    name: 'recentlyPlay',
                    component: () => import('../views/recentlyPlay/index.vue')
                },
                {
                    path: '/audition',
                    name: 'audition',
                    component: () => import('../views/audition/index.vue')
                },
                {
                    path: '/purchased',
                    name: 'purchased',
                    component: () => import('../views/purchased/index.vue')
                },
                {
                    path: '/search',
                    name: 'search',
                    component: () => import('../views/search/index.vue')
                },
                {
                    path: '/singerInformation',
                    name: 'singerInformation',
                    component: () => import('../views/singerInformation/index.vue')
                },
                {
                    path: '/personalInformation',
                    name: 'personalInformation',
                    component: () => import('../views/personalInformation/index.vue')
                },
                {
                    path: '/mv',
                    name: 'mv',
                    component: () => import('../components/mv/index.vue')
                }
            ]
        },
        {
            path: '/lyric',
            name: 'lyric',
            component: () => import('../views/lyric/index.vue')
        }
    ]
})
router.beforeEach((to, from) => {
    NProgress.start()
    return true
})

router.afterEach(() => {
    NProgress.done()
})
export default router
