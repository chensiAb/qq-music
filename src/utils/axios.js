import axios from 'axios'

const service = axios.create({
    baseURL: '/api',
    timeout: 8000,
    withCredentials: true
})

// 请求拦截器
service.interceptors.request.use((config) => {
    config.headers.token = 'token'
    return config
})

// 响应拦截器
service.interceptors.response.use(
    (res) => {
        if (res.status == 200) {
            //   console.log('请求成功', res.data.data)
        }
        return Promise.resolve(res.data)
    },
    (err) => {
        return Promise.reject(err)
    }
)

export default service
