import '../src/assets/reset.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import axios from './utils/axios'
import 'animate.css'
import 'swiper/css'
import lazyPlugin from 'vue3-lazy'
const app = createApp(App)
const pinia = createPinia()

pinia.use(piniaPluginPersistedstate)
lazyPlugin.install(app, {
    loading: '../src/assets/logo.svg',
    error: 'error.png'
})
app.use(ElementPlus)
app.provide('$axios', axios)
app.use(router)
app.use(pinia).mount('#app')
