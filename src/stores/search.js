import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useSearchStore = defineStore(
    'search',
    () => {
        let searchValue = ref('')
        let searchList = ref([])

        return {
            searchValue,
            searchList
        }
    },
    {
        persist: true
    }
)
