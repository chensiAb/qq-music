import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useSingerInformationStore = defineStore(
    'singerInformation',
    () => {
        // 歌手id
        let id = ref('')
        return {
            id
        }
    },
    {
        persist: true
    }
)
