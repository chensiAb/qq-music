import { ref } from 'vue'
import { defineStore } from 'pinia'
import url from '../utils/musicUrl'
import { ElMessage } from 'element-plus'

export const useMusicListStore = defineStore(
    'music',
    () => {
        // 播放的音乐数组
        const musicList = ref([]) // 定义数据
        // 设置音乐数组
        const increment = (item) => {
            musicList.value = item
        }
        // 当前歌曲id
        let id = ref(0)

        // 开始播放
        let playerFlag = ref(true)
        let player = ref(null)
        const toPaly = () => {
            let musicId = id.value
            if (player.value != null) {
                if (playerFlag.value) {
                    player.value.play()
                    playerFlag.value = false
                } else {
                    player.value.pause()
                    playerFlag.value = true
                }
            } else {
                player.value = new Audio(url(musicId))
                player.value.load()
                player.value.play()
                player.value.volume = volumeValue.value / 100
                filtersName()
                playerFlag.value = false

                player.value.addEventListener('timeupdate', function () {
                    if (player.value != null) {
                        ;(currentTime.value = player.value.currentTime),
                            (duration.value = player.value.duration)
                    }
                })
                player.value.addEventListener('ended', function () {
                    if (player.value.ended) {
                        next()
                    }
                })
                player.value.addEventListener('error', function () {
                    ElMessage.error('播放资源无效，自动为您播下一首')
                    next()
                })
                // player.value.addEventListener('canplaythrough', function () {
                //   // alert('渲染成功后会执行的回调函数，一旦渲染成功过，逻辑可以卸载这里')
                // })
            }
        }

        // 当前第几首歌曲 下标
        let count = ref(0)
        // 下一首
        const next = () => {
            if (count.value < musicList.value.dailySongs.length - 1) {
                count.value++
            } else {
                count.value = 0
            }
            if (playOrder.value == 2) {
                count.value--
            }
            if (player.value != null) {
                player.value.pause()
                player.value = null
            }
            playOrderChange()

            let musicId = id.value
            player.value = new Audio(url(musicId))
            player.value.load()
            player.value.play()
            player.value.volume = volumeValue.value / 100
            filtersName()
            playerFlag.value = false

            musicList.value.dailySongs.forEach((item, index) => {
                if (count.value == index) {
                    musicList.value.dailySongs[index]['isPlay'] = true
                } else {
                    musicList.value.dailySongs[index]['isPlay'] = false
                }
            })
            player.value.addEventListener('timeupdate', function () {
                if (player.value != null) {
                    ;(currentTime.value = player.value.currentTime),
                        (duration.value = player.value.duration)
                }
            })
            player.value.addEventListener('ended', function () {
                if (player.value.ended) {
                    next()
                }
            })
            player.value.addEventListener('error', function () {
                ElMessage.error('播放资源无效，自动为您播下一首')
                next()
            })
        }

        // 上一曲
        const previous = () => {
            if (count.value == 0) {
                count.value = musicList.value.dailySongs.length
            }
            if (count.value > 0) {
                count.value--
            }
            if (playOrder.value == 2) {
                count.value++
            }
            if (player.value) {
                player.value.pause()
                player.value = null
            }
            playOrderChange()

            let musicId = id.value
            player.value = new Audio(url(musicId))
            player.value.load()
            player.value.play()
            player.value.volume = volumeValue.value / 100
            filtersName()
            playerFlag.value = false

            musicList.value.dailySongs.forEach((item, index) => {
                if (count.value == index) {
                    musicList.value.dailySongs[index]['isPlay'] = true
                } else {
                    musicList.value.dailySongs[index]['isPlay'] = false
                }
            })
            player.value.addEventListener('timeupdate', function () {
                if (player.value != null) {
                    ;(currentTime.value = player.value.currentTime),
                        (duration.value = player.value.duration)
                }
            })
            player.value.addEventListener('ended', function () {
                if (player.value.ended) {
                    next()
                }
            })
            player.value.addEventListener('error', function () {
                ElMessage.error('播放资源无效，自动为您播上一首')
                previous()
            })
        }
        // 初始化id和歌手歌名
        // 根据id找到歌曲和歌手
        let musicName = ref('null')
        let musicAuthor = ref('null')
        let musicImg = ref('')
        const init = () => {
            id.value = musicList.value.dailySongs[0].id
            filtersName()
        }
        function filtersName() {
            musicList.value?.dailySongs?.forEach((item) => {
                if (id.value == item.id) {
                    musicName.value = item.name
                    musicAuthor.value = item.ar[0].name
                    musicImg.value = item.al.picUrl
                }
            })
        }
        // 播放顺序   2单曲 顺序1 随机0
        let playOrder = ref(1)
        function playOrderChange() {
            if (playOrder.value == 2) {
                id.value = id.value
            }
            if (playOrder.value == 1) {
                id.value = musicList.value.dailySongs[count.value].id
            }
            if (playOrder.value == 0) {
                count.value = Math.floor(Math.random() * musicList.value.dailySongs.length)
                id.value = musicList.value.dailySongs[count.value].id
            }
        }

        // 播放音量
        let volumeValue = ref(100)
        // 当前时间-总时间
        let currentTime = ref(0)
        let duration = ref(0)
        // 歌词数组
        let lyricList = ref([])
        let lyricListIndex = ref(0)
        //是否显示歌词
        let showLyricFlag = ref(false)

        return {
            musicList,
            increment,
            id,
            next,
            previous,
            init,
            musicName,
            musicAuthor,
            musicImg,
            volumeValue,
            currentTime,
            duration,
            player,
            toPaly,
            playerFlag,
            playOrder,
            count,
            lyricList,
            lyricListIndex,
            showLyricFlag
        } // 返回
    }
    // {
    //   persist: true
    // }
)
