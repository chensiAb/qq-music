import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useLoginStore = defineStore(
    'login',
    () => {
        // 是否展示登录页
        let isLogin = ref(false)
        // 用户名
        let userName = ref('')
        // 用户头像
        let avatarUrl = ref('')
        // 用户数据
        let profile = ref('')
        let account = ref('')
        let cookie = ref('')
        // 关注信息
        let follows = ref({})
        // 等级信息等
        let userLevel = ref({})
        // user/subcount  信息 , 歌单，收藏，mv, dj 数量
        let userSubcount = ref({})
        return {
            isLogin,
            userName,
            avatarUrl,
            cookie,
            profile,
            account,
            userLevel,
            userSubcount,
            follows
        }
    },
    {
        persist: true
    }
)
