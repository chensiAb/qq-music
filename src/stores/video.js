import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useVideoStore = defineStore(
    'video',
    () => {
        let url = ref('')
        let mvid = ref('')
        return { url, mvid }
    },
    {
        persist: true
    }
)
